
$(document).ready(function () {

    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $(".tracking .dropdown-menu li a").click(function () {
        let button_class= $(this).parents(".dropdown-menu").data("button");
        $("."+button_class).show();
        $("."+button_class).text($(this).text());
    });
});

function draw_chart() {
    var labels = ["Faster", "On Time", "Overdue"];
    var bgColor = ["#2ecc71", "#f1c40f", "#e74c3c"];
    var data = [
        {labels: labels, datasets: [{backgroundColor: bgColor,
                    data: [31, 58, 13]
                }]
        },
        {labels: labels, datasets: [{backgroundColor: bgColor,
                    data: [21, 48, 33]
                }]
        },
        {labels: labels, datasets: [{backgroundColor: bgColor,
                    data: [0, 25, 75]
                }]
        },
        {labels: labels, datasets: [{backgroundColor: bgColor,
                    data: [70, 17, 13]
                }]
        },
        {labels: labels, datasets: [{backgroundColor: bgColor,
                    data: [35, 24, 41]
                }]
        },
        {labels: labels, datasets: [{backgroundColor: bgColor,
                    data: [12, 56, 32]
                }]
        },
        {labels: labels, datasets: [{backgroundColor: bgColor,
                    data: [23, 45, 32]
                }]
        },
        {labels: labels, datasets: [{backgroundColor: bgColor,
                    data: [70, 17, 13]
                }]
        },
        {labels: labels, datasets: [{backgroundColor: bgColor,
                    data: [35, 24, 41]
                }]
        }
    ];
    for (i = 0; i < 8; i++) {
        var ctx = document.getElementById("myChart" + (i + 1));
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: data[i],
            options: {
                legend: {
                    display: false
                }
            }
        });
    }
}